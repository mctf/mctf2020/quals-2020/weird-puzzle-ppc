package ru.mctf;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Box {

    public static final int SIZE = Field.SIZE * Field.SIZE;

    private final Field[] fields = new Field[Field.SIZE];

    public Box() {
        for (int i = 0; i < Field.SIZE; i++) {
            fields[i] = new Field();
        }
    }

    public Field getField(int pos) {
        return fields[pos];
    }

    public Set<Integer> getUsedNumbersInLine(int line) {
        Set<Integer> used = new HashSet<>();

        for (Field field : fields) {
            int val = field.get(line);
            if (val != Field.UNSET) {
                used.add(val);
            }
        }

        return used;
    }

    public void print() throws IOException {
        for (int i = 0; i < fields.length; i++) {
            System.out.println("Field " + i + "\n");
            fields[i].print();
        }
    }

    public void print(FileWriter fileWriter) throws IOException {
        for (int i = 0; i < fields.length; i++) {
            fileWriter.write("Field " + i + "\n");
            fields[i].print(fileWriter);
        }
    }

    public boolean validate() {
        for (int i = 0; i < Field.SIZE; i++) {
            if (getField(i).getUsedNumbers().size() != Field.SIZE) {
                return false;
            }
        }

        for (int i = 0; i < Field.SIZE; i++) {
            if (getUsedNumbersInLine(i).size() != Field.SIZE) {
                return false;
            }
        }

        return true;
    }

    public Box merge(Box another) {
        Box result = new Box();

        for (int fieldNum = 0; fieldNum < Field.SIZE; fieldNum++) {
            Field thisField = getField(fieldNum);
            Field anotherField = another.getField(fieldNum);
            Field resultField = result.getField(fieldNum);

            for (int elementNum = 0; elementNum < Field.SIZE; elementNum++) {
                int val = thisField.get(elementNum);
                if (val != Field.UNSET) {
                    resultField.set(elementNum, val);
                } else {
                    resultField.set(elementNum, anotherField.get(elementNum));
                }
            }
        }

        return result;
    }

    public Box copy() {
        Box newBox = new Box();
        for (int i = 0; i < newBox.fields.length; i++) {
            newBox.fields[i].mimic(fields[i]);
        }
        return newBox;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Box box = (Box) o;

        return Arrays.equals(fields, box.fields);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(fields);
    }

}
