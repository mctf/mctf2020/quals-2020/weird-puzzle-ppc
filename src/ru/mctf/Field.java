package ru.mctf;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Field {

    public static final int UNSET = -1;
    public static final int SIZE = 36;

    public static final char[] ALPHABET = "abcdefghijklmnopqrstuvwxyz{}_0123456".toCharArray();

    static {
        assert new String(ALPHABET).chars().distinct().count() == ALPHABET.length;
        assert ALPHABET.length == SIZE;
    }

    private final int[] elements = new int[SIZE];

    public Field() {
        Arrays.fill(elements, UNSET);
    }

    public void set(int pos, int value) {
        elements[pos] = value;
    }

    public int get(int pos) {
        return elements[pos];
    }

    public void setFromAlphabet(int pos, char c) {
        for (int i = 0; i < ALPHABET.length; i++) {
            if (ALPHABET[i] == c) {
                set(pos, i + 1);
                return;
            }
        }
    }

    public Set<Integer> getUsedNumbers() {
        Set<Integer> used = new HashSet<>();

        for (Integer n : elements) {
            if (n != UNSET) {
                used.add(n);
            }
        }

        return used;
    }

    public void print() {
        int side = (int) Math.sqrt(SIZE);

        for (int i = 0; i < SIZE; i++) {
            int element = elements[i];
            System.out.print(getElementForPrint(element) + " ");

            if (i != 0 && (i + 1) % side == 0) {
                System.out.println();
            }
        }

        System.out.println();
    }

    public void print(FileWriter fileWriter) throws IOException {
        int side = (int) Math.sqrt(SIZE);

        for (int i = 0; i < SIZE; i++) {
            int element = elements[i];
            fileWriter.write(getElementForPrint(element) + " ");

            if (i != 0 && (i + 1) % side == 0) {
                fileWriter.write("\n");
            }
        }

        fileWriter.write("\n");
    }

    private char getElementForPrint(int element) {
        if (element == UNSET) {
            return '*';
        } else {
            return ALPHABET[element - 1];
        }
    }

    public void mimic(Field another) {
        System.arraycopy(another.elements, 0, elements, 0, Field.SIZE);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Field field = (Field) o;

        return Arrays.equals(elements, field.elements);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(elements);
    }
}
